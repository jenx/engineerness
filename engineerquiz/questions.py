QUESTIONS = {
    "What color is your terminal background?": {
        "my what": 0,
        "DARK LIKE MY SOUL": 10,
        "Brightish.": 0,
        "Probably the same as my nonterminal background": 7,
        "Whatever works *shrug*": 3,
    },
    "How often do you read software documentation?": {
        "read what": 5,
        "Never. 'Tis a sign of weakness.": 10,
        "Whenever I need to look something up?": 3,
        "I don't believe in documentation.": 10,
        "I not only read documentation, I also sometimes write it!": 0,
    },
    "What's your WPM?": {
        "my what": 0,
        "OVER 9000": 10,
        "Good enough for me *shrug*": 3,
        "Could be better": 3,
        "I have people writing my code for me": 7,
    },
    "Do you like the fact that this quiz is zero-indexed?": {
        "don't care": 0,
        "0-indexing is the only way. I approve.": 10,
        "is what?": 0,
        "Whatever works *shrug*": 3,
        "Ah, I didn't even notice": 0,
    },
    "Do you like going to programming meetups and/or conferences?": {
        "Depends": 5,
        "Yes!": 0,
        "Not really": 10,
    },
    "Do you cringe when you look at your own code from a year ago?": {
        "dunno": 3,
        "I'd probably find a couple of things to cringe about.": 5,
        "No.": 10,
    },
    "Do you use the right tools (c) to streamline (c) your processes (c)?": {
        "What?": 5,
        "As long as it makes my life easier, sure": 5,
        "No.": 5,
        "Yes I love JIRA!": 5,
    },
    "Do you sometimes make mistakes?": {
        "Yes.": 0,
    },
    "What's your take on functional languages?": {
        "*excitedly starts talking about the Turing completeness of lambda calculus*": 10,
        "They're cool I guess": 3,
        "I like using functions, they make code better": 0,
        "No opinion": 0,
    },
}
