# The Engineerness Test

Until now, the internet has been largely divided on what makes one an exceptional engineer. Too many opinions. Too many
hot takes. Too many questionable recommendations to follow.

That doesn't really change now.

I've just collected some of them into a quiz.


## Getting the Quiz

It's recommended -- but not necessary -- to run `engineerquiz` in a virtual environment:

```bash
$ python3 -m venv quiz.env
$ source quiz.env/bin/activate
```

To install `engineerquiz` run:

```bash
(quiz) $ pip install engineerquiz
```


## Running the Quiz

```bash
(quiz) $ engineerquiz
```
